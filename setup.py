#!/usr/bin/env python

from distutils.core import setup

setup(name='mhstoeg_rpi_scripts',
      version='1.0.4',
      description='Raspberry Pi Scripts',
      author='Mark Hans Stoeghofer',
      author_email='mhstoeg@gmail.com',
      url='https://gitlab.com/mhstoeg/raspberry-pi-python-scripts.git',
      packages=['mhstoeg_led', 'mhstoeg_linux', 'mhstoeg_util'],
      )
