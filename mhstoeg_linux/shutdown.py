# !/usr/bin/env python

"""
A python script for shutting down the Raspberry Pi
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

import subprocess
import time
from mhstoeg_util import rpi_gpio


def shutdown(pin: int) -> None:
    """
    Will shut down the RPi if the value read from the pin is zero and a state change occurred
    :param pin: The pin used to detect state a change to shut down the RPi
    :return: None
    """
    rpi_gpio.initialise()
    rpi_gpio.setup_pin(pin, rpi_gpio.IO.INPUT, pull_up_down=rpi_gpio.PullUpDown.UP)

    running = True

    previousState = True
    while running:
        currentState = rpi_gpio.get_input(pin)

        # states need to be different to enforce only ONE shutdown call everytime the pin state is changed
        # otherwise, for example, if a button is used to change the pins state, holding it down will keep making
        # unnecessary shutdown calls
        if currentState != previousState and not currentState:
            subprocess.call("init 0", shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

        previousState = currentState

        time.sleep(0.1)


def shutdown_wakeup() -> None:
    """
    Will shut down the RPi if the value read from the pin is zero and a state change occurred. Uses pin 3 to enable
    wake up functionality, since if pin 3 is shorted to ground, the RPi will wake up from a halt state.
    :return: None
    """

    pin = 3  # if pin 3 (using BCM numbering) is shorted, the system will wake up from halt state
    shutdown(pin)
