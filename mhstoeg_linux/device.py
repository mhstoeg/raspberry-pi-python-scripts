#!/usr/bin/env python

"""
A python script containing various functions for interacting the Linux Devices.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.1"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

from mhstoeg_linux.filesystem import find_files


def find_devices(path: str, targets: list) -> list:
    """
    Finds all devices in path that start with a string in targets
    :param path: The path to search for devices
    :param targets: A list of strings to determine if devices exist that start with a string in target
    :return: A list of found devices
    """
    found_devices = find_files(path, targets, str.startswith)
    return found_devices
