#!/usr/bin/env python

"""
A python script containing various functions for interacting the Linux filesystem.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.1"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

import os


def find_files(path: str, targets: list, match_func: callable, recursive_search: bool = False) -> list:
    """
    Finds all files in 'path' that satisfies the match_func against 'targets'. Can Recurse into
    every directory found in 'path' if recursive_search is set to True
    :param path: A string of the path to search
    :param targets: A list of strings to determine file matches that satisfy the match_func
    :param match_func: A function that accepts two strings, returns true if string one satisfies
    the conditions of string two
    :param recursive_search: A boolean stating whether all directories in path and their child directories
    should also be searched using recursion
    :return: A list containing the filepaths of all the files that satisfied the match function against
    targets
    """

    found_files = []
    search_queue = []
    valid_funcs = [str.startswith, str.endswith, str.__eq__]

    if match_func not in valid_funcs:
        raise Exception("Invalid Function")

    for file in os.listdir(path):
        filepath = path + "/" + file
        if recursive_search and os.path.isdir(filepath):
            search_queue.append(filepath)
        else:
            for target in targets:
                if match_func(file, target):
                    if file.replace(" ", "") is not file:  # Contains spaces in the name
                        filepath = path + "/'" + file + "'"    # Add quotes so linux understand the path in more cases
                    found_files.append(filepath)

    while search_queue:
        found_files += find_files(search_queue.pop(0), targets, match_func, True)

    return found_files


def create_directory(path: str, directory: str) -> None:
    """
    Creates a directory at the specified path if it does not exist
    :param path: The path to create the directory at if it exists
    :param directory: The name of the directory to be created
    :return: None
    """
    if os.path.isdir(path) and not os.path.isdir(path + "/" + directory):
        os.mkdir(path + "/" + directory)


def remove_directory(path: str) -> None:
    """
    Removes a directory at the specified path if it exists
    :param path: The path of the directory to remove if it exists
    :return: None
    """
    if os.path.isdir(path):
        os.rmdir(path)
