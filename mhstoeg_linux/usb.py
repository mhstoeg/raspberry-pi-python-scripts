#!/usr/bin/env python

"""
A python script containing various functions for interacting with USB devices on Linux.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.1"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

import os
from mhstoeg_linux.device import find_devices
from mhstoeg_linux.filesystem import create_directory


def find_usb_devices(find_partitions: bool = False) -> list:
    """
    Finds all usb devices or partition filesystem links
    :param: A boolean stating whether to find usb partition links or disk links instead. Default disk links
    :return: A list containing all usb device or partition filesystem links
    """

    found_devices = find_devices("/dev/disk/by-id", ["usb"])
    usb_linked_devices = []

    # Since usb ids were found, they need to be converted to their linked filesystem paths
    for device in found_devices:
        device = os.path.realpath(device)
        # Since linux devices are assigned numbers, we can filter out the found device types whose last digit does not
        # have a number
        eligible = not device[len(device) - 1].isdigit()

        if find_partitions:
            eligible = not eligible

        if eligible:
            usb_linked_devices.append(device)

    return usb_linked_devices


def mount_usb(device: str, path: str, target: str) -> None:
    """
    Mount usb device at path at directory target
    :param device: A string of the device path of the usb to mount
    :param path: A string of the path to mount the usb at directory target
    :param target: The directory to mount the usb device to
    :return: None
    """
    create_directory(path, target)
    os.system("mount " + device + " " + path + "/" + target)


def mount_usb_devices(devices: list, path: str, target: str) -> None:
    """
    Mount usb devices at path at directory target-i, where i is an assigned number
    :param devices: A list of the device paths of usb devices to mount
    :param path: A string pertaining to the path to mount the usb at directory target
    :param target: The directory to mount the usb device to (assigning numbers for each device)
    :return: None
    """

    for i in range(len(devices)):
        device = devices[i]
        mount_usb(device, path, target + "-" + str(i))


def unmount_usb(device: str) -> None:
    """
    Unmount usb device if device path is mounted
    :param device: The device path of the usb to unmount
    :return: None
    """
    os.system("umount " + device)


def unmount_usb_devices(devices: list) -> None:
    """
    Unmount usb devices if their device paths exist
    :param devices: A list of usb device paths
    :return: None
    """
    for device in devices:
        unmount_usb(device)
