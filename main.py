#!/usr/bin/env python

from mhstoeg_linux import usb

usb.unmount_usb('/dev/sd1')
usb.mount_usb('/dev/sda1', '/tmp', 'test')
usb.unmount_usb('/dev/sda1')
