# Raspberry Pi Python Scripts

## Description
Various python scripts for the Raspberry Pi 4, to control hardware and perform tasks automatically on Linux

## Installation
Unix/MacOS:

1. ```Enter the directory```

2. ```python3 -m build```

3. ```sudo python setup.py install```

Windows:

## Usage

## Support
For any queries contact the author at:

Email Address: ```mhstoeg@gmail.com```

## Authors and acknowledgment
Mark Hans Stoeghofer

## License
MIT License

## Project status
Ongoing, new scripts will be added as needed by the author for their hobbyist Raspberry Pi Projects.