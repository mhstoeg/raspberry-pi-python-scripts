#!/usr/bin/env python

"""
A python script containing functions and classes for controlling RPi pins.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

import RPi.GPIO as GPIO
from enum import Enum


# TODO Not all RPi.GPIO functionality is supported through this module as of yet.
class IO(Enum):
    """
    Enum Class containing values for either INPUT or OUTPUT
    """
    INPUT = 0
    OUTPUT = 1


class PullUpDown(Enum):
    """
    Enum Class containing values for RPI GPIO pull_up_down
    """
    UP = 1
    DOWN = 2


def initialise() -> None:
    """
    Initialises Rpi GPIO using BCM numbering and turning off warnings
    :return:
    """
    GPIO.setmode(GPIO.BCM)
    GPIO.setwarnings(False)


def setup_pin(pin: int, io: IO, **kwargs) -> None:
    """
    Sets up RPi GPIO pin as output as LOW using BCM numbering
    :param pin: The RPi GPIO pin to be setup
    :param io: IO enum value, selecting whether to set up pin as INPUT or OUTPUT
    :keyword pull_up_down: The PullUPDown enum value to set pin as if set up as INPUT
    :return: None
    """

    pull_up_down = kwargs.get('pull_up_down', None)

    if io == IO.OUTPUT:
        if pull_up_down:
            raise Exception("Output cannot be set with Pull Up or Down")
        GPIO.setup(pin, GPIO.OUT)
    elif io == IO.INPUT:
        if pull_up_down:
            if pull_up_down == PullUpDown.UP:
                GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_UP)
            elif pull_up_down == PullUpDown.DOWN:
                GPIO.setup(pin, GPIO.IN, pull_up_down=GPIO.PUD_DOWN)
        else:
            GPIO.setup(pin, GPIO.IN)


def setup_pins(pins: list, io: IO, **kwargs) -> None:
    """
    Setups up RPi GPIO pins from list 'pins' as output as LOW using BCM numbering
    :param pins: A list containing the RPi GPIO pins to be setup
    :param io: IO enum value, selecting whether to set up pins as INPUT or OUTPUT
    :keyword pull_up_down: The PullUPDown enum value to set pins as if set up as INPUT
    :return: None
    """

    pull_up_down = kwargs.get('pull_up_down', None)
    for pin in pins:
        if pin is not None:
            setup_pin(pin, io, pull_up_down=pull_up_down)


def set_output_high(pin: int) -> None:
    """
    Sets a RPi OUTPUT pin to HIGH
    :param pin: The RPi GPIO pin to be set to HIGH
    :return: None
    """
    GPIO.output(pin, GPIO.HIGH)


def set_output_low(pin: int) -> None:
    """
    Sets a RPi OUTPUT pin to LOW
    :param pin: The RPi GPIO pins to be turned off
    :return: None
    """

    GPIO.output(pin, GPIO.LOW)


def get_input(pin: int) -> int:
    """
    Reads input from pin
    :param pin: pin to read input
    :return: int val read from input pin
    """
    return GPIO.input(pin)


def turn_off_pins(pins: list) -> None:
    """
    Turns off RPi GPIO pins from list 'pins' by setting output pins as LOW
    :param pins: A list containing the RPi GPIO pins to be turned off
    :return: None
    """
    for pin in pins:
        if pin is not None:
            set_output_low(pin)


def cleanup() -> None:
    """
    Calls the RPi GPIO cleanup function
    :return: None
    """
    GPIO.cleanup()


class Pwm:
    """
    Class used to store and manipulate a RPi.GPIO.PWM Object
    """

    def __init__(self, pin: int, frequency: int) -> None:
        """
         Creates a GPIO.PWM (Pulse Width Modulation) object using 'frequency', attaching it to 'pin'
        :param pin: RPi GPIO pin that the GPIO.PWM Object is attached to
        :param frequency: Integer to create GPIO.PWM Object with
        """
        self.pwm_object = GPIO.PWM(pin, frequency)

    def start(self, duty: float) -> None:
        """
        Starts the Pwm object with its duty cycle set to 'duty'
        :param duty: The duty cycle of the Pwm object
        :return: None
        """
        self.pwm_object.start(duty)

    def stop(self) -> None:
        """
        Stops the GPIO.PWM object
        :return: None
        """
        self.pwm_object.stop()

    def change_frequency(self, frequency: int) -> None:
        """
        Change the frequency of Pwm object
        :param frequency: Integer to set pwm_obj frequency to
        :return: None
        """
        self.pwm_object.ChangeFrequency(frequency)

    def change_duty_cycle(self, duty: float) -> None:
        """
        Change the duty cycle of the Pwm object
        :param duty: Float between 0 - 100 to set Pwm duty cycle to
        :return: None
        """
        self.pwm_object.ChangeDutyCycle(duty)
