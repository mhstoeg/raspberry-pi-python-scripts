#!/usr/bin/env python

"""
A python script containing various utility functions.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.0.0"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"


def greater_than_zero_min(lst: list) -> None or int:
    """
    Determines the smallest number in a list that is greater than 0 and returns it, otherwise None is returned
    :param lst: A list of numbers
    :return: The minimum number in a list that is greater than zero, or None if no number greater than zero exists
             within the list
    """
    min_num = None
    for num in lst:
        if num > 0 and (min_num is None or num < min_num):
            min_num = num
    return min_num
