#!/usr/bin/env python

"""
A python script containing functions for driving LEDs.
"""

__author__ = "Mark Hans Stoeghofer"
__copyright__ = ""
__credits__ = []
__license__ = "MIT"
__version__ = "1.2.0"
__maintainer__ = "Mark Hans Stoeghofer"
__email__ = "mhstoeg@gmail.com"
__status__ = "Production"

import time
from mhstoeg_util.util import greater_than_zero_min
from mhstoeg_util import rpi_gpio


def setup_pwm(pins: list, freq: int) -> tuple[rpi_gpio.Pwm or None, rpi_gpio.Pwm or None, rpi_gpio.Pwm or None]:
    """
    Sets up RGB pins using Pulse Width Modulation
    :param pins: A list containing the RPi GPIO pins, in order of Red, Green and Blue
    :param freq: The PWM frequency
    :return: A tuple containing three values of either Any or None
    """

    if len(pins) != 3:
        raise Exception("The list of pins passed does not contain exactly three pins for R, G and B respectively")

    rpi_gpio.initialise()
    rpi_gpio.setup_pins(pins, rpi_gpio.IO.OUTPUT)
    red = None
    green = None
    blue = None

    try:
        if pins[0] is not None:
            red = rpi_gpio.Pwm(pins[0], freq)
            red.start(0)
        if pins[1] is not None:
            green = rpi_gpio.Pwm(pins[1], freq)
            green.start(0)
        if pins[2] is not None:
            blue = rpi_gpio.Pwm(pins[2], freq)
            blue.start(0)
    except Exception:
        raise Exception("Pin values should be None for off, or a GPIO pin in BCM numbering. Must use empty GPIO "
                        "channels!")

    return red, green, blue


def off(red_pin: int, green_pin: int, blue_pin: int) -> None:
    """
    Turns off rgb LED by setting its output to LOW
    :param red_pin: RPi GPIO pin connecting to the red pin of the LED. None if no pin is connected
    :param green_pin: RPi GPIO pin connecting to the green pin of the LED. None if no pin is connected
    :param blue_pin: RPi GPIO pin connecting to the blue pin of the LED. None if no pin is connected
    :return: None
    """
    rpi_gpio.initialise()
    if red_pin is not None:
        rpi_gpio.setup_pin(red_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_low(red_pin)
    if green_pin is not None:
        rpi_gpio.setup_pin(green_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_low(green_pin)
    if blue_pin is not None:
        rpi_gpio.setup_pin(blue_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_low(blue_pin)


def on(red_pin: int, green_pin: int, blue_pin: int) -> None:
    """
    Turns on rgb LED by setting its output to HIGH
    :param red_pin: RPi GPIO pin connecting to the red pin of the LED. None if no pin is connected
    :param green_pin: RPi GPIO pin connecting to the green pin of the LED. None if no pin is connected
    :param blue_pin: RPi GPIO pin connecting to the blue pin of the LED. None if no pin is connected
    :return: None
    """
    rpi_gpio.initialise()
    if red_pin is not None:
        rpi_gpio.setup_pin(red_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_high(red_pin)
    if green_pin is not None:
        rpi_gpio.setup_pin(green_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_high(green_pin)
    if blue_pin is not None:
        rpi_gpio.setup_pin(blue_pin, rpi_gpio.IO.OUTPUT)
        rpi_gpio.set_output_high(blue_pin)


def on_pwm(red_pin: int, green_pin: int, blue_pin: int, red_val: int, green_val: int, blue_val: int) -> None:
    """
    Controls an RGB LED with varying intensities of RGB values using Pulse Width Modulation. Also works with single
    and dual colour LEDs, pins that do not exist should be specified as None
    :param red_pin: RPi GPIO pin connecting to the red pin of the LED. None if no pin is connected
    :param green_pin: RPi GPIO pin connecting to the green pin of the LED. None if no pin is connected
    :param blue_pin: RPi GPIO pin connecting to the blue pin of the LED. None if no pin is connected
    :param red_val: The max intensity of the red light from 0 -> 255
    :param green_val: The max intensity of the green light from 0 -> 255
    :param blue_val: The max intensity of the blue light from 0 -> 255
    :return: None
    """
    pins = [red_pin, green_pin, blue_pin]
    pwm_freq = 200
    red, green, blue = setup_pwm(pins, pwm_freq)

    rgb_vals = [red_val, green_val, blue_val]
    if min(rgb_vals) < 0 or max(rgb_vals) > 255:
        raise Exception("RGB values must have a value from 0 to 255")

    running = True

    try:
        while running:
            factor = 2.55
            if red is not None:
                red.change_duty_cycle(red_val / factor)
            if green is not None:
                green.change_duty_cycle(green_val / factor)
            if blue is not None:
                blue.change_duty_cycle(blue_val / factor)

    except KeyboardInterrupt:
        if red is not None:
            red.stop()
        if green is not None:
            green.stop()
        if blue is not None:
            blue.stop()
        rpi_gpio.cleanup()


def blink(red_pin: int, green_pin: int, blue_pin: int, red_val: int, green_val: int, blue_val: int,
          freq: int, fade: bool) -> None:
    """
    Controls an RGB LED to blink with varying intensities of RGB values and blink frequencies using Pulse Width
    Modulation. Blinking can be set to fade. Also works with single and dual colour LEDs, pins that do not exist should be
    specified as None
    :param red_pin: RPi GPIO pin connecting to the red pin of the LED. None if no pin is connected
    :param green_pin: RPi GPIO pin connecting to the green pin of the LED. None if no pin is connected
    :param blue_pin: RPi GPIO pin connecting to the blue pin of the LED. None if no pin is connected
    :param red_val: The max intensity of the red light from 0 -> 255
    :param green_val: The max intensity of the green light from 0 -> 255
    :param blue_val: The max intensity of the blue light from 0 -> 255
    :param freq: Number of times the LED will blink per second
    :param fade: Whether to fade the blink or not
    :return: None
    """
    pins = [red_pin, green_pin, blue_pin]
    pwm_freq = 200
    rgb_min = 0
    rgb_values = [0, 0, 0]
    rgb_maxes = [red_val, green_val, blue_val]
    if fade:
        num_of_steps = greater_than_zero_min(rgb_maxes)
    else:
        num_of_steps = 1

    # Handle incorrectly passed values
    if num_of_steps is None:
        raise Exception("RGB LED has not been supplied any duty value for red, green or blue")
    if min(rgb_maxes) < 0 or max(rgb_maxes) > 255:
        raise Exception("RGB values must have a value from 0 to 255")
    if freq < 0:
        raise Exception("Frequency should be equal or greater than zero")

    # steps contain the values and directions (direction determined by value being pos or neg) of each RGB value to
    # correctly maintain colour mixing ratios when incrementing duty cycle using pwm
    # the minimum number of increments in duty cycle is the lowest RGB Value. e.g. for RGB 255 255 5 will have 6
    # increments between 255 255 5 and 0 0 0
    steps = [rgb_maxes[0] / num_of_steps, rgb_maxes[1] / num_of_steps, rgb_maxes[2] / num_of_steps]
    red, green, blue = setup_pwm(pins, pwm_freq)

    current_time = time.time()
    time_passed = 0
    running = True

    try:
        while running:
            if freq > 0:
                last_time = current_time
                current_time = time.time()
                time_passed += current_time - last_time

                # The time for a single increment to occur. Divided by 2, since the time per blink
                #  is off -> on + on -> off, therefore there are twice the num_of_steps per blink.
                step_time = 1 / (freq * num_of_steps * 2)

                if time_passed >= step_time:
                    time_passed = 0

                    for i in range(len(pins)):
                        rgb_values[i] += steps[i]
                        duty = rgb_values[i]
                        duty_max = rgb_maxes[i]

                        if duty >= duty_max:
                            rgb_values[i] = duty_max
                            steps[i] *= -1

                        elif duty <= rgb_min:
                            rgb_values[i] = rgb_min
                            steps[i] *= -1

                factor = 2.55
                if red is not None:
                    red.change_duty_cycle(rgb_values[0] / factor)
                if green is not None:
                    green.change_duty_cycle(rgb_values[1] / factor)
                if blue is not None:
                    blue.change_duty_cycle(rgb_values[2] / factor)

    except KeyboardInterrupt:

        if red is not None:
            red.stop()
        if green is not None:
            green.stop()
        if blue is not None:
            blue.stop()
        rpi_gpio.cleanup()
